# Plan del taller

1. Presentación de ponentes y LIDSOL
2. Comunidad Python e implementaciones
3. Preparación del ambiente de desarrollo
	* ¿Necesitamos un mini taller de git/GitHub?
	```python
			if(grupo.git_knowlage()):
					continue
			else:
					grupo.get_git_course(mode="turbo")
	```
4. Retos
5. Más allá del código: Otra forma de contribuir
6. Cazando bugs

# Sobre nosotros
\begin{multicols}{2}
	\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{img/umoqnier.png}
	\end{figure}

	\columnbreak

	\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{img/luisfi.png}
	\end{figure}		

\end{multicols}

# LIDSOL
\begin{center}
  \includegraphics[width=1.0\textwidth]{img/lidsol-250x250.png}
\end{center}


# Comunidad

* Python Software Fundation
	* [`https://www.python.org/psf/`](https://www.python.org/psf/)
*  Grupo de usuarios locales
	* www.meetup.com
	![Comunidades](img/comunidades.png){height=70%}

# Comunidad Python

* [https://www.python.org/community/](https://www.python.org/community/)

\begin{center}
  \includegraphics[width=1\textwidth]{img/comunidad.png}
\end{center}

# Eventos

* PyCon {US, LATAM}
* DjangoCon
* PyData
* PyDay

# Comunicación y Ayuda

## Slack
* [pyslackers.com](https://pyslackers.com)

## IRC
* Elegir cliente IRC [`https://www.irchelp.org/clients/`](https://www.irchelp.org/clients/)
* Registrarse a Freenode
* #python para preguntas generales
* #python-dev para desarrolladores de CPython
* #disutils para discusión acerca de los paquetes

## Listas de Correo
* [`www.python.org/community/lists/`](www.python.org/community/lists/)

# Comunicación y Ayuda

## Python Weekly
* [`pythonweekly.com`](https://pythonweekly.com)

## Foros
* [`python-forum.io`](https://python-forum.io)
* Reddit: [`/r/learnpython/`](https://reddit.com/r/learnpython)

## Wiki
* [`wiki.python.org`](wiki.python.org)

# Conocimientos

* Python
* Git
* GitHub
* Sistema Operativo
    * Windows
    * GNU/Linux
    * MacOS

# Historia

* Inicia su implementación en 1989

![\tiny Guido van Rossum](img/guido.jpg){height=60%}

# Historia

* 1991 primera versión liberada: 0.9.0
* 1994 primer foro de discusión: versión 1.0
* 2000 versión 1.6.1 compatible con GPL
* 2001 PSF version 2.1 lincencia nueva PSFL

# Python
* Lenguaje híbrido
  * Compilación -> bytecode (.pyc)
  * Runtime (Máquina Virtual)

![\tiny https://indianpythonista.wordpress.com/2018/01/05/demystifying-pyc-files/](img/hibrido.png){height=40%}

# CPython
* "Oficial"
* Implementación de referencia
* Escrito en lenguaje C

\begin{center}
\includegraphics[width=0.25\textwidth]{img/python-logo.png}
\end{center}

# Componentes CPython
* Lenguaje  (Gramática)

## EBNF

```
...
del stmt: 'del' exprlist
pass_stmt: 'pass'
flow_stmt: break_stmt | continue_stmt | return_stmt
break_stmt: 'break'
...
```

# Componentes CPython    
* Compilador (Lexer, Parser, AST)

\begin{center}
\includegraphics[width=0.7\textwidth]{img/compilador.png}
\end{center}

# Componentes CPython
* Runtime (.pyc)

![\tiny https://hownot2code.com/2016/12/22/awesome-bits/](img/bits.jpg){height=45%}

# Alternativas
* Jython
    * Máquina Virtual de Java

\begin{center}
\includegraphics[width=0.55\textwidth]{img/jython.png}
\end{center}

# Alternativas
* IronPython
    * Plataforma .NET

\begin{center}
\includegraphics[width=0.7\textwidth]{img/ironpython.png}
\end{center}

# Alternativas
* Pypy
    * ¡Python en Python!
    * JIT

\begin{center}
\includegraphics[width=0.7\textwidth]{img/pypy.png}
\end{center}

# Ambiente de desarrollo: Windows
* Instalar Visual Studio 2019
* Seleccionar el paquete de Python Development
    * Seleccionar paquete de desarrollo nativo de Python
    * Seleccionar Python 3

# Ambiente de desarrollo: GNU/Linux
* Instalar make, gcc, configure y pkgconfig

## Fedora
```
$ sudo yum install yum-utils
```

## Debian y Ubuntu
```
$ sudo apt install build-essential
```

# Ambiente de desarrollo: GNU/Linux
* Instalar dependencias

## Fedora
```
$ sudo yum-builddep python3
```

## Debian y Ubuntu
```
$ sudo apt install libssl-dev zlib1g-dev \
    libncurses5-dev libncursesw5-dev libreadline-dev \
    libsqlite3-dev libgdbm-dev libdb5.3-dev \
    libbz2-dev libexpat1-dev liblzma-dev libffi-dev
```

# Ambiente de desarrollo: MacOS

## Herramientas para C, git y make
```
$ xcode-select --install
```

## OpenSSL para descargar paquetes
```
$ brew install openssl xz zlib
```

# Registro GitHub
* Visitar la página
	* [https://github.com/](https://github.com/)
* Ir a `Sign Up`
	* Colocar datos

![\tiny Formulario de Registro](img/github-registro.png){height=50%}

# Configuración de git
* Iniciar un emulador de terminal

## 1. Configurar nombre
```
$ git configure --global user.name "Poner Nombre Aqui"
```

## 2. Configurar correo
```
$ git configure --global user.email mi@correo.com
```

# Fork a Python

## 1. Ir a página del repositorio
* [https://github.com/python/cpython](https://github.com/python/cpython)

## 2. Copiar proyecto
* Dar click en `Fork` y esperar  

# Fork a Python

\begin{center}
\includegraphics[width=0.9\textwidth]{img/fork.png}
\end{center}

# Configuración del repositorio local
## 1. Clonar repositorio
```
$ git clone git@github.com:username/cpython.git
```
Cambiar `username` por tu usuario de github

## 2. Añadir proyecto original (para actualizar)
```
$ git remote add upstream git@github.com:python/cpython.git
```

## 3. Crear rama para modificar
```
$ git checkout -b pruebas
```

# Estructura de directorios

```
cpython/
|
|--Python   <- Código fuente del interptrete CPython
|--Lib     	<- Bibliotecas estándar escritas en Python
|--PC      	<- Archivos para construcción en Windows
|--PCbuild 	<- Archivos para construcción en Windows viejos
|--Programs <- Fuente del ejecutable Python y otros binarios
|--Tools    <- Herramientas de construcción y extensión
|--Modules 	<- Bibliotecas estándar escritas en C
|--Doc     	<- Codigo fuente de la documentacion
|--Objects 	<- Tipos base y el modelo de objetos
|--Mac      <- Archivos de soporte para macOS
|--Grammar 	<- Definición del lenguaje como EBNF
|--Parser  	<- Parser de código fuente en Python
|--Misc     <- Archivos miscelaneos
|__Include	<- Archivos de cabeceras de C
```

# Construcción del binario: GNU/Linux
En un emulador de terminal ubicarse en el repositorio
con el comando `cd`

## Configurar construcción
```
$ ./configure --with-pydebug
```

## Construir
```
$ make -j2 -s
```

## Iniciar
```
$ ./python
```

# Construcción del binario: MacOS
En un emulador de terminal ubicarse en el repositorio
con el comando `cd`

## Configurar construcción
```
$ CPPFLAGS="-I$(brew --prefix zlib)/include" \
LDFLAGS="-L$(brew --prefix zlib)/lib" \
./configure --with-openssl=$(brew --prefix openssl) \
--with-pydebug
```

## Construir
```
$ make -j2 -s
```

## Iniciar
```
$ ./python.exe
```

# Reconstrucción del binario
> Cada que se modifica el código en C hay que reconstruir el binario con


\begin{center}
\LARGE{\$ make -j2 -s}
\end{center}


# Ejemplo: Modificación el prompt

En este ejemplo se agrega un mensaje al encabezado inicial del interprete.

```
$ ./python
Python 3.9.0a0 (heads/master-dirty:d565fb9828, Oct 15 2019,\
20:09:10) 
[GCC 9.2.1 20190827 (Red Hat 9.2.1-1)] on linux
[El sentido de la vida, el universo y todo lo demas = 42] <==
Type "help", "copyright", "credits" or "license" for more \
information.
>>> 
```

# 1. Reto: Agrega tu nombre al prompt

![Hackerman by shiiftyshift](img/hackerman.png)

# Zen de Python

El Zen de Python en una serie de aforismos que plasman los principios de diseño
que debería tener cualquier programa escrito en Python

```python
>>> import this

The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
...
```

# 2. Reto: Modificar el Zen de Python

El reto consiste en modificar el Zen de Python

```python
>>> import this
The Zen of Python, by Tim Peters, Diego Barriga 
and Luis Vilchis

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
...
```

## **PISTA:**
`from codecs import encode`

# Solución

* Ir al archivo `Lib/this.py`
* Agregar el texto que se desee
	* El Zen está codificado con un cifrado Cesar de 13 posiciones == 'rot13'
	* Utilizaremos f-strings y la biblioteca `encode`
* Ejecutar `import this` en el interprete
	* ¿Porqué ya no tenemos que re-compilar el binario?

# 3. Reto: Agrega una palabra reservada nueva

## **NOTA:** *Regenerar la grámatica al hacer modificaciones*
	* `$ make regen-grammar`

# Solución

* Ir al archivo `Grammar/Grammar`
* Agregar la palabra reservada siguiendo notación *EBNF*
* Ejecutar `$ make regen-grammar`

# Cazando Bugs

* Issue Tracker: `bugs.python.org`

## Formas de contribución
* Reporte de Bugs
	*	Revisión, comentarios y comprobación de reportes
* Documentación
* Evangelización
	* Dar cursos
	* Resolver dudas
	* Grupos de estudios
* Convertirte en core developer

# Bugs y más Bugs

* Correr la suite de pruebas de python

```
./python -m test -j -rW
./python -m test -h
```
* Si tienes "suerte", verifica el módulo que tiene el error y busca si nadie lo
ha reportado en bpo
```
./python -m test -j -v <modulo_con_error>
```

# Vamos despacio pero Vamos

* ["Easy Issues"](https://bugs.python.org/issue?status=1&@sort=-activity&@columns=id%2Cactivity%2Ctitle%2Ccreator%2Cstatus&@dispname=Easy%20issues&@startwith=0&@group=priority&keywords=6&@action=search&@filter=&@pagesize=50)
* ["Issues con Patch"](https://bugs.python.org/issue?status=1&@sort=-activity&@columns=id%2Cactivity%2Ctitle%2Ccreator%2Cstatus&@dispname=Issues%20with%20patch&@startwith=0&@group=priority&keywords=2&@action=search&@filter=&@pagesize=50)
* ["Newcomer friendly"](https://bugs.python.org/issue?%40search_text=&ignore=file%3Acontent&title=&%40columns=title&id=&%40columns=id&stage=&creation=&creator=&activity=&%40columns=activity&%40sort=activity&actor=&nosy=&type=&components=&versions=&dependencies=&assignee=&keywords=22&priority=&status=1&%40columns=status&resolution=&nosy_count=&message_count=&%40group=&%40pagesize=50&%40startwith=0&%40sortdir=on&%40queryname=&%40old-queryname=&%40action=search)

# Si aun no se programar ¿Qué procede?

* No se necesita saber programar para poder contribuir a la comunidad de python
* El lenguaje pone énfasis en la legibilidad y en la documentación del Código
* Su guía está muy trabajada por toda la comunidad (Para algunos idiomas)

## Documentaciones
* Para desarrolladorxs: `https://github.com/python/devguide`
* Para usuarixs: `https://docs.python.org/3/`

# Traducción de la Documentación
* El PEP 545 dice que cada traducción completa debe tener:
	* Una etiqueta (ISO 639-1): es, pt-br, fr, de, en, ...
	* Distribuida como [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.es)
	* Albergada en `https://github.com/python/python-docs-<LANGUAGE_TAG>`
	* Debe incluir: `tutorial/`, `library/stdtypes`, y `library/functions` *TODO: Investigar*
* Está disponible en `https://docs.python.org/<LANGUAGE_TAG>/<PYTHON_VERSION>/`

# Tenemos un problema

* **https://docs.python.org/es/3.7/**

![404](img/404.png)

## Traducción al español

`https://github.com/raulcd/python-docs.es/`

# Algoritmo para cazar bugs

1. Encuentra un bug susceptible de ser exterminado
	* `>> source_code.find(bug, type='easy')`
2. Crea una rama para atacar el problema 
	* `$ git checkout -b bpo-24537`
3. Implementa y documenta tu solución  
	* `$ git add . && git commit "fix(core): Solucion a problema crítico"`
4. Haz un pull request  
	* `$ git push origin bpo-24537 && pull-request`
5. Ajusta tu solución de acuerdo a la retroalimentación
	* `$ git reset -- hard`
6. Repite desde el paso 3
	* `goto 3`

# Importante, debes vender tu alma

\begin{center}
\LARGE{https://www.python.org/psf/contrib/}
\end{center}


---
title: ¿Cómo contribuir a `CPython`?
author:
- Diego A. Barriga Martínez (@umoqnier)\newline
- Luis Alberto Oropeza Vilchis (@vilchisfi)
institute: Laboratorio de Investigación y Desarrollo de Software Libre
logo: 'img/lidsol.png'
theme: Boadilla 
colortheme: default
date: "19 de Octubre 2019"
navigation: horizontal
header-includes:
- \usepackage{caption}
- \captionsetup[figure]{labelformat=empty}
- \usepackage{multicol}
output:
  beamer_presentation:
    slide_level: 2
---
